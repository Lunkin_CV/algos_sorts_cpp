#include <iostream>
#include <random>
#include <limits.h>
#include <ctime>
#include <getopt.h>
#include <string.h>
#include "config.hpp"

using namespace std;

int check_sorts = 0; //global variable to signalize that sorts' results must be checked

int main(int argc, const char * argv[]) {
    //vectors of tasks
    vector<AutoTask<ArraySequence<int>>*> a_ar_vc;
    vector<FileTask<ArraySequence<int>>*> f_ar_vc;
    vector<ManualTask<ArraySequence<int>>*> m_ar_vc;
    vector<AutoTask<ListSequence<int>>*> a_ls_vc;
    vector<FileTask<ListSequence<int>>*> f_ls_vc;
    vector<ManualTask<ListSequence<int>>*> m_ls_vc;
    try {
        Execute(&a_ar_vc, &f_ar_vc, &m_ar_vc, &a_ls_vc, &f_ls_vc, &m_ls_vc, argc, argv);
    } catch (const runtime_error& re) {
        cout << "Runtime error: " << re.what() << endl;
    } catch (const exception& ex) {
        cout << "Error occurred: " << ex.what() << endl;
    } catch (...) {
        cout << "Unknown failure occurred. Possible memory corruption" << endl;
    }
    return 0;
}
