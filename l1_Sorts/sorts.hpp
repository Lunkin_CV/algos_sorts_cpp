#ifndef sorts_hpp
#define sorts_hpp

#include <stdio.h>
#include <stdlib.h>
#include <random>
#include <stack>
#include "array_sequence.hpp"
#include "list_sequence.hpp"

extern int check_sorts; //global variable to signalize that sorts' results must be checked

template <typename TSeq>
void BubbleSort(TSeq* seq, int time_limit, int check) {
    if (seq == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (time_limit < 0) {
        string er = "Time limit is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    
    clock_t start_time = clock();
    long long int len = (*seq).GetLength();
    if (len == 0) {
        return;
    }
    int swapped;
    do {
        swapped = 0;
        for (long long int i = 0; i < len - 1; i++) {
            if ((*seq).Cmp(i, i + 1) > 0) {
                (*seq).Swap(i, i + 1);
                swapped = 1;
            }
        }
        if (time_limit != 0 && int(double(clock() - start_time) / CLOCKS_PER_SEC) > time_limit) {
            throw runtime_error("time_limit");
        }
    } while (swapped == 1);
    
    //checking the sort's result
    if (check) {
        if (IsCorrect(seq) == 0) {
            string er = "Sort was incorrect in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
    }
}

/*!
 * @discussion pivot will be chosen randomly from sequence elements. simple partition, non-recursive.
 */
template <typename TSeq>
void QuickSort(TSeq* seq, int time_limit, int check) {
    if (seq == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (time_limit < 0) {
        string er = "Time limit is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    
    if ((*seq).GetLength() == 0) {
        return;
    }
    
    clock_t start_time = clock();
    long long int l_i, r_i, pivot_i, start_i, end_i;
    std::stack<long long int> index_st;
    std::random_device rd;
    std::mt19937_64 gen(rd());
    
    index_st.push((*seq).GetLength() - 1);
    index_st.push(0);
    
    while (index_st.size() > 1) {
        //prepairing indexes and pivot
        start_i = index_st.top();
        index_st.pop();
        end_i = index_st.top();
        index_st.pop();
        
        if (end_i == start_i) {
            return;
        }
        if ((time_limit > 0) && (double(clock() - start_time) / CLOCKS_PER_SEC > double(time_limit))) {
            throw runtime_error("time_limit");
        }
        std::uniform_int_distribution<long long int> distr(start_i, end_i);
        pivot_i = distr(gen);
        l_i = start_i;
        r_i = end_i;
        
        //partitioning
        while (l_i <= r_i) {
            
            while ((*seq).Cmp(l_i, pivot_i) < 0) {
                l_i++;
            }
            while ((*seq).Cmp(pivot_i, r_i) < 0) {
                r_i--;
            }
            if (l_i <= r_i) {
                (*seq).Swap(l_i, r_i);
                if (l_i == pivot_i) {
                    pivot_i = r_i;
                } else if (r_i == pivot_i){
                    pivot_i = l_i;
                }
                l_i++;
                r_i--;
            }
        }
        //recursive calls
        if (start_i < r_i) {
            index_st.push(r_i);
            index_st.push(start_i);
        }
        if (end_i > l_i) {
            index_st.push(end_i);
            index_st.push(l_i);
        }
    }
    //checking the sort's result
    if (check) {
        if (IsCorrect(seq) == 0) {
            string er = "Sort was incorrect in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
    }
}

/*!
 * @discussion non-recursive
 */
template <typename TSeq>
void MaxHeapify(TSeq* seq, long long int i, long long int heap_size){
    if (seq == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (i >= (*seq).GetLength()) {
        string er = "Index is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (heap_size > (*seq).GetLength()) {
        string er = "heap_size is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (i < 0) {
        string er = "Index is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if ((*seq).GetLength() == 0) {
        return;
    }
    
    long long int l = i * 2 + 1;
    long long int r = l + 1;
    long long int largest = i;
    while (largest < heap_size) {
        largest = i;
        if (l < heap_size && (*seq).Cmp(l, largest) > 0) {
            largest = l;
        }
        if (r < heap_size && (*seq).Cmp(r, largest) > 0) {
            largest = r;
        }
        
        if (largest != i) {
            (*seq).Swap(i, largest);
        } else {
            break;
        }
        i = largest;
        l = i * 2 + 1;
        r = l + 1;
    }
}

template <typename TSeq>
void HeapSort(TSeq* seq, int time_limit, int check) {
    if (seq == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (time_limit < 0) {
        string er = "Time limit is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    
    if ((*seq).GetLength() == 0) {
        return;
    }
    clock_t start_time = clock();
    //creating the heap
    for (long long int i = (*seq).GetLength() / 2 - 1; i >= 0; i--) {
        MaxHeapify(seq, i, (*seq).GetLength());
        if (time_limit != 0 && int(double(clock() - start_time) / CLOCKS_PER_SEC) > time_limit) {
            throw runtime_error("time_limit");
        }
    }
    //the sort itself
    for (long long int i = (*seq).GetLength() - 1; i >= 0; i--) {
        (*seq).Swap(0, i);
        MaxHeapify(seq, 0, i);
        if (time_limit != 0 && int(double(clock() - start_time) / CLOCKS_PER_SEC) > time_limit) {
            throw runtime_error("time_limit");
        }
    }
    //checking the sort's result
    if (check) {
        if (IsCorrect(seq) == 0) {
            string er = "Sort was incorrect in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
    }
}

/*!
 * @discussion checks sequence for correctness
 * @return 1 - correct, 0 - incorrect
 */
template <typename TSeq>
int IsCorrect(TSeq* seq) {
    if (seq == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    
    if ((*seq).GetLength() == 1) {
        return 1;
    }
    for (long long int i = 0; i < (*seq).GetLength() - 1; i++) {
        if (TElCmp((*seq).Get(i), (*seq).Get(i + 1)) > 0) {
            return 0;
        }
    }
    return 1;
}

#endif /* sorts_hpp */
