#ifndef manual_task_hpp
#define manual_task_hpp

#include <stdio.h>
#include <limits.h>
#include "compare.hpp"

template <typename SeqType>
class ManualTask: public Task<SeqType>{
public:
    ManualTask(int sorts,
               int time_limit,
               long long int print_limit,
               string raw_output_path,
               string result_output_path,
               string report_path);
    ~ManualTask();
    void PrintInfo(ostream* str);
    void PrintTimeAndSeq(ostream* str);
    void Make();
    void Run();
    int IsDone();
    int IsReady();
private:
    int sorts_ = QUICK | BUBBLE | HEAP;
    int time_limit_ = 0;
    long long int print_limit_ = 0;
    string raw_output_path_;
    string result_output_path_;
    string report_path_;
    vector<SeqType*> seq_vc_;
    vector<SeqType*> result_seq_vc_;
    vector<results_st> results_;
    int done_ = 0;
    int ready_ = 0;
};

/*!
 * @discussion creates manual task (keyboard input from user).
 */
template <typename SeqType>
ManualTask<SeqType>::ManualTask(int sorts,
                                int time_limit,
                                long long int print_limit,
                                string raw_output_path,
                                string result_output_path,
                                string report_path) {
    if (sorts > QUICK + BUBBLE + HEAP || sorts <= 0) {
        sorts_ = QUICK | BUBBLE | HEAP;
    } else {
        sorts_ = sorts;
    }
    if (time_limit < 0) {
        time_limit_ = 0;
    } else {
        time_limit_ = time_limit;
    }
    print_limit_ = print_limit;
    raw_output_path_ = raw_output_path;
    result_output_path_ = result_output_path;
    report_path_ = report_path;
}

template <typename SeqType>
ManualTask<SeqType>::~ManualTask() {
    for (int i = 0; i < seq_vc_.size(); i++) {
        delete seq_vc_[i];
    }
    for (int i = 0; i < result_seq_vc_.size(); i++) {
        delete result_seq_vc_[i];
    }
}

template <typename SeqType>
int ManualTask<SeqType>::IsReady() {
    return ready_;
}

template <typename SeqType>
int ManualTask<SeqType>::IsDone() {
    return done_;
}

template <typename SeqType>
void ManualTask<SeqType>::PrintInfo(ostream* str) {
    (*str) << "#################### Manual task ####################\n";
    (*str) << "Sequence type: " << typeid(SeqType).name() << "\nSorts: ";
    if ((sorts_ & QUICK) != 0) {
        (*str) << "Quicksort, ";
    }
    if ((sorts_ & BUBBLE) != 0) {
        (*str) << "Bubble sort, ";
    }
    if ((sorts_ & HEAP) != 0) {
        (*str) << "Heapsort, ";
    }
    (*str) << "\n";
    (*str) << "Number of sequences: " << results_.size() << "\n";
    if (time_limit_ != 0) {
        (*str) << "Time limit: " << time_limit_ << " seconds\n";
    }
    if (print_limit_ == 0) {
        (*str) << "Don't print.\n";
    } else if (print_limit_ > 0) {
        (*str) << "Print sequences with less than " << print_limit_ + 1 << " elements.\n";
    } else {
        (*str) << "Print all sequences.\n";
    }
    if (report_path_.empty() == false) {
        (*str) << "Report path: " << report_path_ << "\n";
    }
    if (raw_output_path_.empty() == false) {
        (*str) << "Unsorted sequences path: " << raw_output_path_ << "\n";
    }
    if (result_output_path_.empty() == false) {
        (*str) << "Result sequences path: " << result_output_path_ << "\n";
    }
    (*str) << "Status: ";
    if (done_ == 1) {
        (*str) << "DONE\n";
    } else if (ready_ == 1){
        (*str) << "READY\n";
    } else {
        (*str) << "NOT READY\n";
    }
    (*str) << "#####################################################\n";
}

/*!
 * @discussion takes sequences from user and prepares task for running
 */
template <typename SeqType>
void ManualTask<SeqType>::Make() {
    if (ready_ == 1) {
        return;
    }
    PrintInfo(&cout);
    cout << "Now start to input sequences.\nAfter each new member press Enter.\nFor a new sequence type \"new\" and press Enter.\nAfter all sequences type \"end\" and press Enter.\nMembers can be from " << INT_MIN << " to " << INT_MAX << ".\n";
    SeqType* new_seq;
    try {
        new_seq = new SeqType();
    } catch (std::bad_alloc& ba) {
        string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    string line;
    cout << ">";
    while (getline(cin, line)) {
        if (line.compare("end") == 0) {
            if ((*new_seq).GetLength() > 0) {
                seq_vc_.push_back(new_seq);
                results_.push_back({-2, -2, -2});
            }
            break;
        } else if (line.compare("new") == 0) {
            //i++;
            if ((*new_seq).GetLength() > 0) {
                seq_vc_.push_back(new_seq);
                results_.push_back({-2, -2, -2});
            }
            try {
                new_seq = new SeqType();
            } catch (std::bad_alloc& ba) {
                string er = "Bad memory allocation in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
            cout << ">";
        } else {
            (*new_seq).Append(stoi(line));
            cout << ">";
        }
    }
    for (int j = 0; j < seq_vc_.size(); j++) {
        try {
            result_seq_vc_.push_back(new SeqType());
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        
    }
    ready_ = 1;
}

template <typename SeqType>
void ManualTask<SeqType>::PrintTimeAndSeq(ostream* str) {
    for (int i = 0; i < results_.size(); i++) {
        (*str) << i + 1 << ".\n";
        if ((sorts_ & QUICK) != 0) {
            (*str) << "Quicksort: ";
            if (results_[i].quick >= 0) {
                (*str) << results_[i].quick << "\n";
            } else if (results_[i].quick == -1){
                (*str) << "time limit reached\n";
            } else {
                (*str) << "not sorted yet\n";
            }
        }
        if ((sorts_ & BUBBLE) != 0) {
            (*str) << "Bubble sort: ";
            if (results_[i].bubble >= 0) {
                (*str) << results_[i].bubble << "\n";
            } else if (results_[i].bubble == -1){
                (*str) << "time limit reached\n";
            } else {
                (*str) << "not sorted yet\n";
            }
        }
        if ((sorts_ & HEAP) != 0) {
            (*str) << "Heapsort: ";
            if (results_[i].heap >= 0) {
                (*str) << results_[i].heap << "\n";
            } else if (results_[i].heap == -1){
                (*str) << "time limit reached\n";
            } else {
                (*str) << "not sorted yet\n";
            }
        }
        if (((*result_seq_vc_[i]).GetLength() > 0 && (*result_seq_vc_[i]).GetLength() <= print_limit_) || print_limit_ < 0) {
            for (long long int j = 0; j < (*result_seq_vc_[i]).GetLength(); j++) {
                (*str) << (*result_seq_vc_[i]).Get(j) << ", ";
            }
            (*str) << "\n";
        }
        (*str) << "\n";
    }
}

/*!
 * @discussion executes the task
 */
template <typename SeqType>
void ManualTask<SeqType>::Run() {
    if (done_ == 1) {
        return;
    }
    clock_t begin, end;
    ofstream raw_file, result_file, report_file;
    int filled_for_print = 0;
    
    if (report_path_.empty() == false) {
        report_file.open(report_path_, ios::out | ios::app);
    }
    if (raw_output_path_.empty() == false) {
        raw_file.open(raw_output_path_, ios::out | ios::app);
    }
    if (result_output_path_.empty() == false) {
        result_file.open(result_output_path_, ios::out | ios::app);
    }
    
    PrintInfo(&cout);
    if (report_file) {
        PrintInfo(&report_file);
    }
    cout << "TASK STARTED\n";
    //exports raw data to file if needed
    if (raw_file) {
        ExportFile(&raw_file, &seq_vc_);
    }
    for (int i = 0; i < seq_vc_.size(); i++) {
        filled_for_print = 0;
        SeqType* test_seq;
        
        for (int j = 1; j <= MAX_SORT; j *= 2) {
            if ((sorts_ & j) != 0) {
                //making test sequence
                try {
                    test_seq = new SeqType((*seq_vc_[i]).GetLength());
                } catch (std::bad_alloc& ba) {
                    string er = "Bad memory allocation in ";
                    er += __PRETTY_FUNCTION__;
                    throw runtime_error(er);
                }
                for (long long int k = 0; k < (*seq_vc_[i]).GetLength(); k++) {
                    (*test_seq).Append((*seq_vc_[i]).Get(k));
                }
                //sort with time limit if needed
                try {
                    begin = clock();
                    if (j == BUBBLE) {
                        BubbleSort(test_seq, time_limit_, check_sorts);
                    } else if (j == QUICK) {
                        QuickSort(test_seq, time_limit_, check_sorts);
                    } else if (j == HEAP) {
                        HeapSort(test_seq, time_limit_, check_sorts);
                    }
                    end = clock();
                    if (j == BUBBLE) {
                        results_[i].bubble = double(end - begin) / CLOCKS_PER_SEC;
                    } else if (j == QUICK) {
                        results_[i].quick = double(end - begin) / CLOCKS_PER_SEC;
                    } else if (j == HEAP) {
                        results_[i].heap = double(end - begin) / CLOCKS_PER_SEC;
                    }
                    
                    if (filled_for_print == 0) {
                        for (long long int j = 0; j < (*test_seq).GetLength(); j++) {
                            (*result_seq_vc_[i]).Append((*test_seq).Get(j));
                        }
                        filled_for_print = 1;
                    }
                    delete test_seq;
                } catch (runtime_error &re) {
                    if (strcmp(re.what(), "time_limit") != 0) {
                        throw re;
                    }
                    if (j == BUBBLE) {
                        results_[i].bubble = -1;
                    } else if (j == QUICK) {
                        results_[i].quick = -1;
                    } else if (j == HEAP) {
                        results_[i].heap = -1;
                    }
                    delete test_seq;
                }
            }
        }
        cout << float(i + 1) * 100 / seq_vc_.size() << "% done.\n";
    }
    done_ = 1;
    //exports result sequences in file if needed
    if (result_file) {
        ExportFile(&result_file, &result_seq_vc_);
        //clear all sequences that won't be printed
        if (print_limit_ >= 0) {
            for (int i = 0; i < results_.size(); i++) {
                if ((*result_seq_vc_[i]).GetLength() > print_limit_) {
                    delete result_seq_vc_[i];
                    try {
                        result_seq_vc_[i] = new SeqType();
                    } catch (std::bad_alloc& ba) {
                        string er = "Bad memory allocation in ";
                        er += __PRETTY_FUNCTION__;
                        throw runtime_error(er);
                    }
                }
            }
        }
    }
    cout << "TASK FINISHED\n";
    PrintTimeAndSeq(&cout);
    if (report_file) {
        PrintTimeAndSeq(&report_file);
    }
    cout << "\n";
    if (report_path_.empty() == false) {
        report_file.close();
    }
    if (raw_output_path_.empty() == false) {
        raw_file.close();
    }
    if (result_output_path_.empty() == false) {
        result_file.close();
    }
}

#endif /* manual_task_hpp */
