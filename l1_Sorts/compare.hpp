#ifndef compare_hpp
#define compare_hpp

#include <stdio.h>
#include <random>
#include <limits.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "sorts.hpp"



#define SORTS_NUM 3 //number of sorts to compare

//IDs of sorts for flag
#define QUICK 1
#define BUBBLE 2
#define HEAP 4
#define MAX_SORT 4

//IDs of modes for flag
#define RANDOM 1
#define UPW 2
#define BCKW 4
#define CRCR 8
#define MAX_MODE 8

using namespace std;

//struct for time results of sorts
struct results_st {
    double bubble;
    double quick;
    double heap;
};

//abstract class for tasks
template <typename SeqType>
class Task {
public:
    virtual void Make() = 0;
    virtual void Run() = 0;
    virtual void PrintInfo(ostream* str) = 0;
    virtual void PrintTimeAndSeq(ostream* str) = 0;
    virtual int IsReady() = 0;
    virtual int IsDone() = 0;
};

/*!
 * @discussion fills in sequence with templates
 */
template <typename SeqType>
void FillInt(SeqType* seq, long long int amnt, int mode) {
    if (seq == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (amnt < 0) {
        string er = "Amount is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    
    //repeat multiplier
    long long int rep = amnt / INT_MAX + 1;
    if (mode == UPW) {
        //upward
        for (long long int i = 0; i < amnt; i++) {
            (*seq).Append((int)(i / rep));
        }
    } else if (mode == BCKW){
        //backward
        for (long long int i = amnt; i > 0; i--) {
            (*seq).Append((int)(i / rep));
        }
    } else if(mode == CRCR){
        //criss-cross
        rep = (rep - 1) / 2 + 1;
        for (long long int i = amnt / 2; i > 0; i--) {
            (*seq).Append((int)(i / rep));
            (*seq).Append((int)((amnt - i) / rep));
        }
        if (amnt % 2 == 1) {
            (*seq).Append(0);
        }
    } else {
        //random
        random_device rd;
        mt19937_64 gen(rd());
        uniform_int_distribution<int> distr;
        for (long long int i = 0; i < amnt; i++) {
            (*seq).Append(distr(gen));
        }
    }
}

/*!
 * @discussion exports new set of data from file's line to sequence
 */
template <typename SeqType>
void ImportLine(ifstream* str, SeqType* seq) {
    if (seq == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    
    if (*str) {
        string line, token;
        stringstream iss;
        if (getline(*str, line))
        {
            iss << line;
            while (getline(iss, token, ';'))
            {
                (*seq).Append(stoi(token));
            }
        }
    } else {
        string er = "bad_file";
        throw invalid_argument(er);
    }
}

/*!
 * @discussion exports new sets of data from file to sequence
 */
template <typename SeqType>
void ImportFile(ifstream* str, vector<SeqType*>* vc) {
    if (!(*str)) {
        string er = "bad_file";
        throw invalid_argument(er);
    }
    if (vc == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    
    while (*str) {
        try {
            SeqType* seq = new SeqType();
            ImportLine(str, seq);
            if ((*seq).GetIsEmpty() == 0) {
                (*vc).push_back(seq);
            }
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
    }
}

/*!
 * @discussion imports sets of data from sequence to file
 */
template <typename SeqType>
void ExportFile(ofstream* str, vector<SeqType*>* vc) {
    if (!(*str)) {
        string er = "bad_file";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (vc == nullptr) {
        string er = "NULL pointer occurred in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    
    for (int i = 0; i < (*vc).size(); i++) {
        long long int len = (*(*vc)[i]).GetLength();
        for (long long int j = 0; j < len - 1; j++) {
            (*str) << (*(*vc)[i]).Get(j) << ';';
        }
        (*str) << (*(*vc)[i]).Get(len - 1) << '\n';
    }
}



#endif /* compare_hpp */
