#ifndef config_hpp
#define config_hpp

#include <stdio.h>
#include <random>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "auto_task.hpp"
#include "file_task.hpp"
#include "manual_task.hpp"

//path to configuration file
#define CONFIG_PATH "config.csv"

#define HELP "#################### OPERATION MANUAL ####################\n\
This program allows you to compare Bubble sort Quicksort and Heapsort on your own sequences of int numbers (from file or keyboard) and generated sequences (random, upward, backward and criss-cross). \n\
It will generate a report with results and, if you want, save sorted and generated sequences.\n\n\
There are two types of keys: major keys and minor keys.\n\
Major key tells the program what to do. In each command you must use only one major key.\n\
Minor key adds details. However, sometimes minor keys are required.\n\
So the general form of a command is:\n\
./l1_Sorts --major-key --minor-key1 param --minor-key2 --param1,param2,param3\n\n\
This program uses task system. The program stores all tasks in configuration file config.csv.\n\
There are three types of tasks: auto tasks, file tasks, manual tasks. \n\
Each of them can operate with sequences based on either arrays or lists.\n\
Auto tasks generate sequences for sorts by themselves (random, upward, backward and criss-cross).\
Required minor keys: --amount\n\
File tasks take sequences from .csv files with ';' delimiter. One line for one sequence.\
Required minor keys: --raw-path\n\
Manual tasks take sequences from user via terminal. No required minor keys.\n\
\n\
List of major keys with their minor keys:\n\
--help Shows this manual.\n\
--list Shows list of current tasks.\n\
--clear-tasks Deletes all tasks.\n\
--run Executes all tasks.\n\
    [--quiet] Disables terminal output.\n\
    [--check] Sorts' results will be checked. In incorrect case you will see error message.\n\
--add-auto-array Add new auto task with sequences based on arrays.\n\
--add-auto-list Add new auto task with sequences based on lists.\n\
--add-file-array Add new file task with sequences based on arrays.\n\
--add-file-list Add new file task with sequences based on lists.\n\
--add-manual-array Add new manual task with sequences based on arrays.\n\
--add-manual-list Add new manual task with sequences based on lists.\n\
    [--sorts sort1,sort2] List of sorts for task. Default: q,b,h.\n\
        q - Quicksort\n\
        b - Bubble sort\n\
        h - Heapsort\n\
        Default: q,b,h.\n\
    [--time-limit N] Time limit for each sort in seconds. 0 for no time limit. Default is 0.\n\
    [--print-limit N] Maximal number of result sequence members to be printed in terminal. 0 to disable. -1 for unlimited.\n\
    {--raw-path PATH} Path to file with/for unsorted sequences.\n\
    [--result-path PATH] Path to file for sorted sequences.\n\
    [--report-path PATH] Path to file for report.\n\
    [--modes mode1,mode2] Only for auto tasks. List of modes for sequence generation.\n\
        r - random\n\
        u - upward\n\
        b - backward\n\
        c - criss-cross\n\
        Default: r,u,b,c\n\
    --amount am1,am2 Only for auto tasks. Amounts for sequnce generation.\n\
"

using namespace std;

extern int check_sorts; //global variable to signalize that sorts' results must be checked

/*!
 * @discussion loads tasks from configuration file
 */
void LoadConfig(vector<AutoTask<ArraySequence<int>>*>* a_ar_vc,
                vector<FileTask<ArraySequence<int>>*>* f_ar_vc,
                vector<ManualTask<ArraySequence<int>>*>* m_ar_vc,
                vector<AutoTask<ListSequence<int>>*>* a_ls_vc,
                vector<FileTask<ListSequence<int>>*>* f_ls_vc,
                vector<ManualTask<ListSequence<int>>*>* m_ls_vc) {
    if (a_ar_vc == nullptr ||
        f_ar_vc == nullptr ||
        m_ar_vc == nullptr ||
        a_ls_vc == nullptr ||
        f_ls_vc == nullptr ||
        m_ls_vc == nullptr) {
        string er = "NULL pointer occured in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    ifstream config_file;
    config_file.open(CONFIG_PATH);
    if (config_file) {
        while (config_file) {
            string line, token; //string for file's line and string for element in csv
            stringstream iss;
            //getting new line from file
            if (getline(config_file, line)) {
                string task_type, seq_type;
                int sorts; //sorts flag
                int time_limit;
                long long int print_limit;
                string raw_output_path;
                string result_output_path;
                string report_path;
                int modes; //modes flag
                vector<long long int> amount;
                
                //sending new line to stream
                iss << line;
                //getting settings from line via stream
                getline(iss, task_type, ';');
                getline(iss, seq_type, ';');
                getline(iss, token, ';');
                sorts = stoi(token);
                getline(iss, token, ';');
                time_limit = stoi(token);
                getline(iss, token, ';');
                print_limit = stoll(token);
                getline(iss, raw_output_path, ';');
                getline(iss, result_output_path, ';');
                getline(iss, report_path, ';');
                //setting new task
                if (task_type.compare("a") == 0) {
                    //getting additional settings
                    getline(iss, token, ';');
                    modes = stoi(token);
                    while (getline(iss, token, ';')) {
                        amount.push_back(stoll(token));
                    }
                    if (seq_type.compare("ar") == 0) {
                        try {
                            (*a_ar_vc).push_back(new AutoTask<ArraySequence<int>>(sorts,
                                                                                  time_limit,
                                                                                  print_limit,
                                                                                  raw_output_path,
                                                                                  result_output_path,
                                                                                  report_path,
                                                                                  modes,
                                                                                  amount));
                        } catch (std::bad_alloc& ba) {
                            string er = "Bad memory allocation in ";
                            er += __PRETTY_FUNCTION__;
                            throw runtime_error(er);
                        }
                    } else if (seq_type.compare("ls") == 0) {
                        try {
                            (*a_ls_vc).push_back(new AutoTask<ListSequence<int>>(sorts,
                                                                                 time_limit,
                                                                                 print_limit,
                                                                                 raw_output_path,
                                                                                 result_output_path,
                                                                                 report_path,
                                                                                 modes,
                                                                                 amount));
                        } catch (std::bad_alloc& ba) {
                            string er = "Bad memory allocation in ";
                            er += __PRETTY_FUNCTION__;
                            throw runtime_error(er);
                        }
                    }
                    amount.clear();
                } else if (task_type.compare("f") == 0) {
                    if (seq_type.compare("ar") == 0) {
                        try {
                            (*f_ar_vc).push_back(new FileTask<ArraySequence<int>>(sorts,
                                                                                  time_limit,
                                                                                  print_limit,
                                                                                  raw_output_path,
                                                                                  result_output_path,
                                                                                  report_path));
                        } catch (std::bad_alloc& ba) {
                            string er = "Bad memory allocation in ";
                            er += __PRETTY_FUNCTION__;
                            throw runtime_error(er);
                        }
                    } else if (seq_type.compare("ls") == 0) {
                        try {
                            (*f_ls_vc).push_back(new FileTask<ListSequence<int>>(sorts,
                                                                                 time_limit,
                                                                                 print_limit,
                                                                                 raw_output_path,
                                                                                 result_output_path,
                                                                                 report_path));
                        } catch (std::bad_alloc& ba) {
                            string er = "Bad memory allocation in ";
                            er += __PRETTY_FUNCTION__;
                            throw runtime_error(er);
                        }
                    }
                } else if (task_type.compare("m") == 0) {
                    if (seq_type.compare("ar") == 0) {
                        try {
                            (*m_ar_vc).push_back(new ManualTask<ArraySequence<int>>(sorts,
                                                                                    time_limit,
                                                                                    print_limit,
                                                                                    raw_output_path,
                                                                                    result_output_path,
                                                                                    report_path));
                        } catch (std::bad_alloc& ba) {
                            string er = "Bad memory allocation in ";
                            er += __PRETTY_FUNCTION__;
                            throw runtime_error(er);
                        }
                    } else if (seq_type.compare("ls") == 0) {
                        try {
                            (*m_ls_vc).push_back(new ManualTask<ListSequence<int>>(sorts,
                                                                                   time_limit,
                                                                                   print_limit,
                                                                                   raw_output_path,
                                                                                   result_output_path,
                                                                                   report_path));
                        } catch (std::bad_alloc& ba) {
                            string er = "Bad memory allocation in ";
                            er += __PRETTY_FUNCTION__;
                            throw runtime_error(er);
                        }
                    }
                }
                
            } else {
                config_file.close();
                return;
            }
        }
    } else {
        config_file.close();
        string er = "Bad config file ";
        er += CONFIG_PATH;
        er += " in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    config_file.close();
}

/*!
 * @discussion prints info about tasks
 */
void PrintInfo(vector<AutoTask<ArraySequence<int>>*>* a_ar_vc,
               vector<FileTask<ArraySequence<int>>*>* f_ar_vc,
               vector<ManualTask<ArraySequence<int>>*>* m_ar_vc,
               vector<AutoTask<ListSequence<int>>*>* a_ls_vc,
               vector<FileTask<ListSequence<int>>*>* f_ls_vc,
               vector<ManualTask<ListSequence<int>>*>* m_ls_vc) {
    if (a_ar_vc == nullptr ||
        f_ar_vc == nullptr ||
        m_ar_vc == nullptr ||
        a_ls_vc == nullptr ||
        f_ls_vc == nullptr ||
        m_ls_vc == nullptr) {
        string er = "NULL pointer occured in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    for (int i = 0; i < (*a_ar_vc).size(); i++) {
        (*(*a_ar_vc)[i]).PrintInfo(&cout);
    }
    for (int i = 0; i < (*f_ar_vc).size(); i++) {
        (*(*f_ar_vc)[i]).PrintInfo(&cout);
    }
    for (int i = 0; i < (*m_ar_vc).size(); i++) {
        (*(*m_ar_vc)[i]).PrintInfo(&cout);
    }
    for (int i = 0; i < (*a_ls_vc).size(); i++) {
        (*(*a_ls_vc)[i]).PrintInfo(&cout);
    }
    for (int i = 0; i < (*f_ls_vc).size(); i++) {
        (*(*f_ls_vc)[i]).PrintInfo(&cout);
    }
    for (int i = 0; i < (*m_ls_vc).size(); i++) {
        (*(*m_ls_vc)[i]).PrintInfo(&cout);
    }
}

/*!
 * @discussion prints results of sorts and result sequences if needed
 */
void PrintTimeAndSeq(vector<AutoTask<ArraySequence<int>>*>* a_ar_vc,
                     vector<FileTask<ArraySequence<int>>*>* f_ar_vc,
                     vector<ManualTask<ArraySequence<int>>*>* m_ar_vc,
                     vector<AutoTask<ListSequence<int>>*>* a_ls_vc,
                     vector<FileTask<ListSequence<int>>*>* f_ls_vc,
                     vector<ManualTask<ListSequence<int>>*>* m_ls_vc) {
    if (a_ar_vc == nullptr ||
        f_ar_vc == nullptr ||
        m_ar_vc == nullptr ||
        a_ls_vc == nullptr ||
        f_ls_vc == nullptr ||
        m_ls_vc == nullptr) {
        string er = "NULL pointer occured in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    for (int i = 0; i < (*a_ar_vc).size(); i++) {
        (*(*a_ar_vc)[i]).PrintTimeAndSeq(&cout);
    }
    for (int i = 0; i < (*f_ar_vc).size(); i++) {
        (*(*f_ar_vc)[i]).PrintTimeAndSeq(&cout);
    }
    for (int i = 0; i < (*m_ar_vc).size(); i++) {
        (*(*m_ar_vc)[i]).PrintTimeAndSeq(&cout);
    }
    for (int i = 0; i < (*a_ls_vc).size(); i++) {
        (*(*a_ls_vc)[i]).PrintTimeAndSeq(&cout);
    }
    for (int i = 0; i < (*f_ls_vc).size(); i++) {
        (*(*f_ls_vc)[i]).PrintTimeAndSeq(&cout);
    }
    for (int i = 0; i < (*m_ls_vc).size(); i++) {
        (*(*m_ls_vc)[i]).PrintTimeAndSeq(&cout);
    }
}

/*!
 * @discussion prepares all the tasks' data
 */
void Make(vector<AutoTask<ArraySequence<int>>*>* a_ar_vc,
          vector<FileTask<ArraySequence<int>>*>* f_ar_vc,
          vector<ManualTask<ArraySequence<int>>*>* m_ar_vc,
          vector<AutoTask<ListSequence<int>>*>* a_ls_vc,
          vector<FileTask<ListSequence<int>>*>* f_ls_vc,
          vector<ManualTask<ListSequence<int>>*>* m_ls_vc) {
    if (a_ar_vc == nullptr ||
        f_ar_vc == nullptr ||
        m_ar_vc == nullptr ||
        a_ls_vc == nullptr ||
        f_ls_vc == nullptr ||
        m_ls_vc == nullptr) {
        string er = "NULL pointer occured in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    for (int i = 0; i < (*a_ar_vc).size(); i++) {
        (*(*a_ar_vc)[i]).Make();
    }
    for (int i = 0; i < (*f_ar_vc).size(); i++) {
        (*(*f_ar_vc)[i]).Make();
    }
    for (int i = 0; i < (*m_ar_vc).size(); i++) {
        (*(*m_ar_vc)[i]).Make();
    }
    for (int i = 0; i < (*a_ls_vc).size(); i++) {
        (*(*a_ls_vc)[i]).Make();
    }
    for (int i = 0; i < (*f_ls_vc).size(); i++) {
        (*(*f_ls_vc)[i]).Make();
    }
    for (int i = 0; i < (*m_ls_vc).size(); i++) {
        (*(*m_ls_vc)[i]).Make();
    }
}

/*!
 * @discussion runs all tasks
 */
void Run(vector<AutoTask<ArraySequence<int>>*>* a_ar_vc,
         vector<FileTask<ArraySequence<int>>*>* f_ar_vc,
         vector<ManualTask<ArraySequence<int>>*>* m_ar_vc,
         vector<AutoTask<ListSequence<int>>*>* a_ls_vc,
         vector<FileTask<ListSequence<int>>*>* f_ls_vc,
         vector<ManualTask<ListSequence<int>>*>* m_ls_vc) {
    if (a_ar_vc == nullptr ||
        f_ar_vc == nullptr ||
        m_ar_vc == nullptr ||
        a_ls_vc == nullptr ||
        f_ls_vc == nullptr ||
        m_ls_vc == nullptr) {
        string er = "NULL pointer occured in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    for (int i = 0; i < (*a_ar_vc).size(); i++) {
        (*(*a_ar_vc)[i]).Run();
    }
    for (int i = 0; i < (*f_ar_vc).size(); i++) {
        (*(*f_ar_vc)[i]).Run();
    }
    for (int i = 0; i < (*m_ar_vc).size(); i++) {
        (*(*m_ar_vc)[i]).Run();
    }
    for (int i = 0; i < (*a_ls_vc).size(); i++) {
        (*(*a_ls_vc)[i]).Run();
    }
    for (int i = 0; i < (*f_ls_vc).size(); i++) {
        (*(*f_ls_vc)[i]).Run();
    }
    for (int i = 0; i < (*m_ls_vc).size(); i++) {
        (*(*m_ls_vc)[i]).Run();
    }
}

/*!
 * @discussion recognizes minor keys for --add-xxx-yyy
 * @param task_type 'a' for auto, 'f' for file, anything else for manual
 * @param line pointer to the string for keys
 */
void ArgAppend(char task_type, string* line, int argc, const char * argv[]) {
    if (line == nullptr || argv == nullptr) {
        string er = "NULL pointer occured in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    
    string token; //temp string
    int sorts = 0; //sorts flag
    int time_limit = 0;
    long long int print_limit = 0;
    string raw_path;
    string result_path;
    string report_path;
    int modes = 0; //modes flag
    vector<long long int> amount;
    //recognition
    for (int i = 2; i + 1 < argc; i += 2) {
        if (strcmp(argv[i], "--sorts") == 0) {
            sorts = 0;
            if (strstr(argv[i + 1], "b") != nullptr) {
                sorts |= BUBBLE;
            }
            if (strstr(argv[i + 1], "q") != nullptr) {
                sorts |= QUICK;
            }
            if (strstr(argv[i + 1], "h") != nullptr) {
                sorts |= HEAP;
            }
        } else if (strcmp(argv[i], "--time-limit") == 0) {
            time_limit = atoi(argv[i + 1]);
            if (time_limit < 0) {
                time_limit = 0;
            }
        } else if (strcmp(argv[i], "--print-limit") == 0) {
            print_limit = atoll(argv[i + 1]);
        } else if (strcmp(argv[i], "--raw-path") == 0) {
            raw_path = argv[i + 1];
        } else if (strcmp(argv[i], "--result-path") == 0) {
            result_path = argv[i + 1];
        } else if (strcmp(argv[i], "--report-path") == 0) {
            report_path = argv[i + 1];
        } else if (task_type == 'a' && strcmp(argv[i], "--modes") == 0) {
            modes = 0;
            if (strstr(argv[i + 1], "r") != nullptr) {
                modes |= RANDOM;
            }
            if (strstr(argv[i + 1], "u") != nullptr) {
                modes |= UPW;
            }
            if (strstr(argv[i + 1], "b") != nullptr) {
                modes |= BCKW;
            }
            if (strstr(argv[i + 1], "c") != nullptr) {
                modes |= CRCR;
            }
        } else if (task_type == 'a' && strcmp(argv[i], "--amount") == 0) {
            amount.clear();
            stringstream iss;
            iss << argv[i + 1];
            while (getline(iss, token, ',')) {
                amount.push_back(stoll(token));
            }
        }
    }
    if (task_type == 'a' && amount.empty() == true) {
        throw invalid_argument("Amount not found. Try --help for instructions.\n");
    }
    if (task_type == 'f' && raw_path.empty() == true) {
        throw invalid_argument("Input file path not found. Try --help for instructions.\n");
    }
    //transfering to the line
    stringstream iss;
    iss << sorts << ";" << time_limit << ";" << print_limit << ";" << raw_path << ";";
    iss << result_path << ";" << report_path;
    if (task_type == 'a') {
        iss << ";" << modes;
        for (int i = 0; i < amount.size(); i++) {
            iss << ";" << amount[i];
        }
    }
    iss << "\n";
    (*line) += iss.str();
}

/*!
 * @discussion executes the ui
 */
void Execute(vector<AutoTask<ArraySequence<int>>*>* a_ar_vc,
             vector<FileTask<ArraySequence<int>>*>* f_ar_vc,
             vector<ManualTask<ArraySequence<int>>*>* m_ar_vc,
             vector<AutoTask<ListSequence<int>>*>* a_ls_vc,
             vector<FileTask<ListSequence<int>>*>* f_ls_vc,
             vector<ManualTask<ListSequence<int>>*>* m_ls_vc,
             int argc,
             const char * argv[]){
    if (a_ar_vc == nullptr ||
        f_ar_vc == nullptr ||
        m_ar_vc == nullptr ||
        a_ls_vc == nullptr ||
        f_ls_vc == nullptr ||
        m_ls_vc == nullptr) {
        string er = "NULL pointer occured in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    //major keys recognition
    if (argc == 1) {
        cout << "Try --help for instructions.\n";
    } else if (strcmp(argv[1], "--help") == 0) {
        cout << HELP;
        cout << "Note that all members of sequences must be from ";
        cout << INT_MIN << " to " << INT_MAX << ".\n";
    } else if (strcmp(argv[1], "--run") == 0) {
        int quiet = 0;
        for (int i = 2; i < argc; i++) {
            //minor keys of --run recognition
            if (strcmp(argv[2], "--quiet") == 0) {
                //disables cout
                cout.setstate(std::ios_base::badbit);
                //setting global variable to signal that cout is disabled
                quiet = 1;
            } else if (strcmp(argv[2], "--check") == 0) {
                //setting global variable to signal that sorts must be checked
                check_sorts = 1;
            }
        }
        cout << ">>LOADING CONFIGURATION FILE\n";
        LoadConfig(a_ar_vc, f_ar_vc, m_ar_vc, a_ls_vc, f_ls_vc, m_ls_vc);
        cout << ">>PREPAIRING THE DATA\n";
        Make(a_ar_vc, f_ar_vc, m_ar_vc, a_ls_vc, f_ls_vc, m_ls_vc);
        cout << ">>STARTING TASKS\n";
        Run(a_ar_vc, f_ar_vc, m_ar_vc, a_ls_vc, f_ls_vc, m_ls_vc);
        cout << ">>ALL DONE\n";
        //enabling cout if needed
        if (quiet == 1) {
            cout.clear();
        }
    } else if (strcmp(argv[1], "--list") == 0) {
        LoadConfig(a_ar_vc, f_ar_vc, m_ar_vc, a_ls_vc, f_ls_vc, m_ls_vc);
        PrintInfo(a_ar_vc, f_ar_vc, m_ar_vc, a_ls_vc, f_ls_vc, m_ls_vc);
    } else if (strcmp(argv[1], "--clear-tasks") == 0) {
        ofstream config_file;
        //trunc will clear the file
        config_file.open(CONFIG_PATH, ios::out | ios::trunc);
        config_file.close();
        cout << ">>CONFIGURATION FILE CLEARED\n";
    } else if (strcmp(argv[1], "--add-auto-array") == 0) {
        string line = "a;ar;";
        //minor keys recognition
        ArgAppend('a', &line, argc, argv);
        ofstream config_file;
        config_file.open(CONFIG_PATH, ios::out | ios::app);
        if (config_file) {
            config_file << line;
        } else {
            line = "Bad config file ";
            line += CONFIG_PATH;
            line += " in ";
            line += __PRETTY_FUNCTION__;
            throw runtime_error(line);
        }
        config_file.close();
    } else if (strcmp(argv[1], "--add-file-array") == 0) {
        string line = "f;ar;";
        //minor keys recognition
        ArgAppend('f', &line, argc, argv);
        ofstream config_file;
        config_file.open(CONFIG_PATH, ios::out | ios::app);
        if (config_file) {
            config_file << line;
        } else {
            line = "Bad config file ";
            line += CONFIG_PATH;
            line += " in ";
            line += __PRETTY_FUNCTION__;
            throw runtime_error(line);
        }
        config_file.close();
    } else if (strcmp(argv[1], "--add-manual-array") == 0) {
        string line = "m;ar;";
        //minor keys recognition
        ArgAppend('m', &line, argc, argv);
        ofstream config_file;
        config_file.open(CONFIG_PATH, ios::out | ios::app);
        if (config_file) {
            config_file << line;
        } else {
            line = "Bad config file ";
            line += CONFIG_PATH;
            line += " in ";
            line += __PRETTY_FUNCTION__;
            throw runtime_error(line);
        }
        config_file.close();
    } else if (strcmp(argv[1], "--add-auto-list") == 0) {
        string line = "a;ls;";
        //minor keys recognition
        ArgAppend('a', &line, argc, argv);
        ofstream config_file;
        config_file.open(CONFIG_PATH, ios::out | ios::app);
        if (config_file) {
            config_file << line;
        } else {
            line = "Bad config file ";
            line += CONFIG_PATH;
            line += " in ";
            line += __PRETTY_FUNCTION__;
            throw runtime_error(line);
        }
        config_file.close();
    } else if (strcmp(argv[1], "--add-file-list") == 0) {
        string line = "f;ls;";
        //minor keys recognition
        ArgAppend('f', &line, argc, argv);
        ofstream config_file;
        config_file.open(CONFIG_PATH, ios::out | ios::app);
        if (config_file) {
            config_file << line;
        } else {
            line = "Bad config file ";
            line += CONFIG_PATH;
            line += " in ";
            line += __PRETTY_FUNCTION__;
            throw runtime_error(line);
        }
        config_file.close();
    } else if (strcmp(argv[1], "--add-manual-list") == 0) {
        //minor keys recognition
        string line = "m;ls;";
        ArgAppend('m', &line, argc, argv);
        ofstream config_file;
        config_file.open(CONFIG_PATH, ios::out | ios::app);
        if (config_file) {
            config_file << line;
        } else {
            line = "Bad config file ";
            line += CONFIG_PATH;
            line += " in ";
            line += __PRETTY_FUNCTION__;
            throw runtime_error(line);
        }
        config_file.close();
    } else {
        cout << "Command not found. Try --help for instructions.\n";
    }
}
#endif /* config_hpp */
