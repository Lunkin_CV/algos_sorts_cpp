#ifndef auto_task_hpp
#define auto_task_hpp

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "compare.hpp"

template <typename SeqType>
class AutoTask: public Task<SeqType>{
public:
    AutoTask(int sorts,
             int time_limit,
             long long int print_limit,
             string raw_output_path,
             string result_output_path,
             string report_path,
             int modes,
             vector<long long int> amount);
    ~AutoTask();
    void Make();
    void Run();
    void PrintInfo(ostream* str);
    void PrintTimeAndSeq(ostream* str);
    int IsReady();
    int IsDone();
private:
    string raw_output_path_;
    string result_output_path_;
    string report_path_;
    int sorts_ = QUICK | BUBBLE | HEAP;
    int modes_ = RANDOM | UPW | BCKW | CRCR;
    int time_limit_ = 0;
    long long int print_limit_ = 0;
    vector<SeqType*> seq_vc_;
    vector<SeqType*> result_seq_vc_;
    vector<long long int> amount_;
    vector<struct results_st> results_;
    int done_ = 0;
    int ready_ = 0;
};

template <typename SeqType>
AutoTask<SeqType>::AutoTask(int sorts,
                            int time_limit,
                            long long int print_limit,
                            string raw_output_path,
                            string result_output_path,
                            string report_path,
                            int modes,
                            vector<long long int> amount) {
    if (amount.size() == 0){
        string er = "No amount in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    for (int i = 0; i < amount.size(); i++) {
        if (amount[i] <= 0) {
            string er = "Amount is zero or negative in ";
            er += __PRETTY_FUNCTION__;
            throw out_of_range(er);
        }
    }
    
    if (sorts > QUICK + BUBBLE + HEAP || sorts <= 0) {
        sorts_ = QUICK | BUBBLE | HEAP;
    } else {
        sorts_ = sorts;
    }
    if (modes > RANDOM + UPW + BCKW + CRCR || modes <= 0) {
        modes_ = RANDOM | UPW | BCKW | CRCR;
    } else {
        modes_ = modes;
    }
    if (time_limit < 0) {
        time_limit_ = 0;
    } else {
        time_limit_ = time_limit;
    }
    print_limit_ = print_limit;
    raw_output_path_ = raw_output_path;
    result_output_path_ = result_output_path;
    report_path_ = report_path;
    amount_ = amount;
}

template <typename SeqType>
AutoTask<SeqType>::~AutoTask() {
    for (int i = 0; i < seq_vc_.size(); i++) {
        delete seq_vc_[i];
    }
    for (int i = 0; i < result_seq_vc_.size(); i++) {
        delete result_seq_vc_[i];
    }
}

template <typename SeqType>
int AutoTask<SeqType>::IsReady() {
    return ready_;
}

template <typename SeqType>
int AutoTask<SeqType>::IsDone() {
    return done_;
}

template <typename SeqType>
void AutoTask<SeqType>::PrintInfo(ostream* str) {
    (*str) << "##################### Auto task #####################\n" << "Sequence type: " << typeid(SeqType).name() << "\nSorts: ";
    if ((sorts_ & QUICK) != 0) {
        (*str) << "Quicksort, ";
    }
    if ((sorts_ & BUBBLE) != 0) {
        (*str) << "Bubble sort, ";
    }
    if ((sorts_ & HEAP) != 0) {
        (*str) << "Heapsort, ";
    }
    (*str) << "\nModes: ";
    if ((modes_ & RANDOM) != 0) {
        (*str) << "random, ";
    }
    if ((modes_ & UPW) != 0) {
        (*str) << "upward, ";
    }
    if ((modes_ & BCKW) != 0) {
        (*str) << "backward, ";
    }
    if ((modes_ & CRCR) != 0) {
        (*str) << "criss-cross, ";
    }
    (*str) << "\n";
    (*str) << "Amounts: ";
    for (int i = 0; i < amount_.size(); i++) {
        (*str) << amount_[i] << ", ";
    }
    (*str) << "\n";
    (*str) << "Number of sequences: " << results_.size() << "\n";
    if (time_limit_ != 0) {
        (*str) << "Time limit: " << time_limit_ << " seconds\n";
    }
    if (print_limit_ == 0) {
        (*str) << "Don't print.\n";
    } else if (print_limit_ > 0) {
        (*str) << "Print sequences with less than " << print_limit_ + 1 << " elements.\n";
    } else {
        (*str) << "Print all sequences.\n";
    }
    if (report_path_.empty() == false) {
        (*str) << "Report path: " << report_path_ << "\n";
    }
    if (raw_output_path_.empty() == false) {
        (*str) << "Unsorted sequences path: " << raw_output_path_ << "\n";
    }
    if (result_output_path_.empty() == false) {
        (*str) << "Result sequences path: " << result_output_path_ << "\n";
    }
    (*str) << "Status: ";
    if (done_ == 1) {
        (*str) << "DONE\n";
    } else if (ready_ == 1){
        (*str) << "READY\n";
    } else {
        (*str) << "NOT READY\n";
    }
    (*str) << "#####################################################\n";
}

template <typename SeqType>
void AutoTask<SeqType>::Make() {
    if (ready_ == 1) {
        return;
    }
    for (int i = 1; i <= MAX_MODE; i *= 2) {
        if ((modes_ & i) != 0) {
            for (int j = 0; j < amount_.size(); j++) {
                try {
                    seq_vc_.push_back(new SeqType());
                    FillInt(seq_vc_.back(), amount_[j], i);
                    results_.push_back({-2, -2, -2});
                    result_seq_vc_.push_back(new SeqType());
                } catch (std::bad_alloc& ba) {
                    string er = "Bad memory allocation in ";
                    er += __PRETTY_FUNCTION__;
                    throw runtime_error(er);
                }
            }
        }
    }
    ready_ = 1;
}

template <typename SeqType>
void AutoTask<SeqType>::PrintTimeAndSeq(ostream* str) {
    int current_mode = 0;
    for (int i = 0; i < results_.size(); i++) {
        
        if (i % amount_.size() == 0) {
            current_mode *= 2;
            if (current_mode == 0) {
                current_mode = 1;
            }
            while ((current_mode & modes_) == 0) {
                current_mode *= 2;
            }
            (*str) << "////////// ";
            if (current_mode == RANDOM) {
                (*str) << "random mode";
            }
            if (current_mode == UPW) {
                (*str) << "upward mode";
            }
            if (current_mode == BCKW) {
                (*str) << "backward mode";
            }
            if (current_mode == CRCR) {
                (*str) << "criss-cross mode";
            }
            (*str) << " //////////\n";
        }
        
        (*str) << i % amount_.size() + 1 << ". Amount: " << amount_[i % amount_.size()] << "\n";
        if ((sorts_ & QUICK) != 0) {
            (*str) << "Quicksort: ";
            if (results_[i].quick >= 0) {
                (*str) << results_[i].quick << "\n";
            } else if (results_[i].quick == -1){
                (*str) << "time limit reached\n";
            } else {
                (*str) << "not sorted yet\n";
            }
        }
        if ((sorts_ & BUBBLE) != 0) {
            (*str) << "Bubble sort: ";
            if (results_[i].bubble >= 0) {
                (*str) << results_[i].bubble << "\n";
            } else if (results_[i].bubble == -1){
                (*str) << "time limit reached\n";
            } else {
                (*str) << "not sorted yet\n";
            }
        }
        if ((sorts_ & HEAP) != 0) {
            (*str) << "Heapsort: ";
            if (results_[i].heap >= 0) {
                (*str) << results_[i].heap << "\n";
            } else if (results_[i].heap == -1){
                (*str) << "time limit reached\n";
            } else {
                (*str) << "not sorted yet\n";
            }
        }
        if (((*result_seq_vc_[i]).GetLength() > 0 && (*result_seq_vc_[i]).GetLength() <= print_limit_) || print_limit_ < 0) {
            for (long long int j = 0; j < (*result_seq_vc_[i]).GetLength(); j++) {
                (*str) << (*result_seq_vc_[i]).Get(j) << ", ";
            }
            (*str) << "\n";
        }
        (*str) << "\n";
    }
}

template <typename SeqType>
void AutoTask<SeqType>::Run() {
    if (done_ == 1) {
        return;
    }
    clock_t begin, end;
    ofstream raw_file, result_file, report_file;
    int filled_for_print = 0;
    
    if (report_path_.empty() == false) {
        report_file.open(report_path_, ios::out | ios::app);
    }
    if (raw_output_path_.empty() == false) {
        raw_file.open(raw_output_path_, ios::out | ios::app);
    }
    if (result_output_path_.empty() == false) {
        result_file.open(result_output_path_, ios::out | ios::app);
    }
    
    PrintInfo(&cout);
    if (report_file) {
        PrintInfo(&report_file);
    }
    cout << "TASK STARTED\n";
    //exports raw data to file if needed
    if (raw_file) {
        ExportFile(&raw_file, &seq_vc_);
    }
    for (int i = 0; i < seq_vc_.size(); i++) {
        filled_for_print = 0;
        SeqType* test_seq;
        
        for (int j = 1; j <= MAX_SORT; j *= 2) {
            if ((sorts_ & j) != 0) {
                //making test sequence
                try {
                    test_seq = new SeqType((*seq_vc_[i]).GetLength());
                } catch (std::bad_alloc& ba) {
                    string er = "Bad memory allocation in ";
                    er += __PRETTY_FUNCTION__;
                    throw runtime_error(er);
                }
                for (long long int k = 0; k < (*seq_vc_[i]).GetLength(); k++) {
                    (*test_seq).Append((*seq_vc_[i]).Get(k));
                }
                //sort with time limit if needed
                try {
                    begin = clock();
                    if (j == BUBBLE) {
                        BubbleSort(test_seq, time_limit_, check_sorts);
                    } else if (j == QUICK) {
                        QuickSort(test_seq, time_limit_, check_sorts);
                    } else if (j == HEAP) {
                        HeapSort(test_seq, time_limit_, check_sorts);
                    }
                    end = clock();
                    if (j == BUBBLE) {
                        results_[i].bubble = double(end - begin) / CLOCKS_PER_SEC;
                    } else if (j == QUICK) {
                        results_[i].quick = double(end - begin) / CLOCKS_PER_SEC;
                    } else if (j == HEAP) {
                        results_[i].heap = double(end - begin) / CLOCKS_PER_SEC;
                    }
                    
                    if (filled_for_print == 0) {
                        for (long long int j = 0; j < (*test_seq).GetLength(); j++) {
                            (*result_seq_vc_[i]).Append((*test_seq).Get(j));
                        }
                        filled_for_print = 1;
                    }
                    delete test_seq;
                } catch (runtime_error &re) {
                    if (strcmp(re.what(), "time_limit") != 0) {
                        throw re;
                    }
                    if (j == BUBBLE) {
                        results_[i].bubble = -1;
                    } else if (j == QUICK) {
                        results_[i].quick = -1;
                    } else if (j == HEAP) {
                        results_[i].heap = -1;
                    }
                    delete test_seq;
                }
            }
        }
        cout << float(i + 1) * 100 / seq_vc_.size() << "% done.\n";
    }
    done_ = 1;
    //exports result sequences in file if needed
    if (result_file) {
        ExportFile(&result_file, &result_seq_vc_);
        //clear all sequences that won't be printed
        if (print_limit_ >= 0) {
            for (int i = 0; i < results_.size(); i++) {
                if ((*result_seq_vc_[i]).GetLength() > print_limit_) {
                    delete result_seq_vc_[i];
                    try {
                        result_seq_vc_[i] = new SeqType();
                    } catch (std::bad_alloc& ba) {
                        string er = "Bad memory allocation in ";
                        er += __PRETTY_FUNCTION__;
                        throw runtime_error(er);
                    }
                }
            }
        }
    }
    cout << "TASK FINISHED\n";
    PrintTimeAndSeq(&cout);
    if (report_file) {
        PrintTimeAndSeq(&report_file);
    }
    cout << "\n";
    if (report_path_.empty() == false) {
        report_file.close();
    }
    if (raw_output_path_.empty() == false) {
        raw_file.close();
    }
    if (result_output_path_.empty() == false) {
        result_file.close();
    }
}

#endif /* auto_task_hpp */
