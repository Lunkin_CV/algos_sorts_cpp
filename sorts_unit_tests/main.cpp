#include <iostream>
#include "sequence.hpp"
#include "array_sequence.hpp"
#include "list_sequence.hpp"
#include "sorts.hpp"
#include "file_task.hpp"

int check = 0;

int main(int argc, const char * argv[]) {
    int correct = 0;
    ArraySequence<int>* test_arr_seq;
    int unsorted[14] = {11, 10, 9, 8, 7, 6, 0, 1, 2, 3, 4, 5, 10, 11};
    int sorted[14] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 11, 11};
    int mono[14] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    std::cout << "Tests for BubbleSort\n";
    try {
        //1
        cout << "1. Sort sequence 11, 10, 9, 8, 7, 6, 0, 1, 2, 3, 4, 5, 10, 11. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        for (int i = 0; i < 14; i++) {
            (*test_arr_seq).Append(unsorted[i]);
        }
        BubbleSort<ArraySequence<int>>(test_arr_seq, 0);
        for (int i = 0; i < 14; i++) {
            if (sorted[i] != (*test_arr_seq).Get(i)) {
                cout << "NOT PASSED\n";
                throw runtime_error("1. BubbleSort is wrong.");
            }
        }
        cout << "PASSED\n";
        delete test_arr_seq;
        
        //2
        cout << "2. NULL sequence pointer. ";
        try {
            correct = 0;
            BubbleSort<ArraySequence<int>>(nullptr, 0);
        } catch (const std::invalid_argument& excp) {
            correct = 1;
        }
        if (correct == 0) {
            cout << "NOT PASSED\n";
            throw runtime_error("2. BubbleSort is wrong. NULL pointer case.");
        }
        cout << "PASSED\n";
        
        //3
        cout << "3. Sort sorted sequence 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 11, 11. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        for (int i = 0; i < 14; i++) {
            (*test_arr_seq).Append(sorted[i]);
        }
        BubbleSort<ArraySequence<int>>(test_arr_seq, 0);
        for (int i = 0; i < 14; i++) {
            if (sorted[i] != (*test_arr_seq).Get(i)) {
                cout << "NOT PASSED\n";
                throw runtime_error("3. BubbleSort is wrong.");
            }
        }
        cout << "PASSED\n";
        delete test_arr_seq;
        
        //4
        cout << "4. Sort mono sequence 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        for (int i = 0; i < 14; i++) {
            (*test_arr_seq).Append(mono[i]);
        }
        BubbleSort<ArraySequence<int>>(test_arr_seq, 0);
        for (int i = 0; i < 14; i++) {
            if (mono[i] != (*test_arr_seq).Get(i)) {
                cout << "NOT PASSED\n";
                throw runtime_error("4. BubbleSort is wrong.");
            }
        }
        cout << "PASSED\n";
        delete test_arr_seq;
        
        //5
        cout << "5. Sort sequence without elements. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        BubbleSort<ArraySequence<int>>(test_arr_seq, 0);
        cout << "PASSED\n";
        delete test_arr_seq;
        
        printf("All tests are passed.\n\n\n");
    } catch (const runtime_error& re) {
        cout << "Runtime error: " << re.what() << endl;
    } catch (const exception& ex) {
        cout << "Error occurred: " << ex.what() << endl;
    } catch (...) {
        cout << "Unknown failure occurred. Possible memory corruption" << endl;
    }

    
    std::cout << "Tests for QuickSort\n";
    try {
        
        //1
        cout << "1. Sort sequence 11, 10, 9, 8, 7, 6, 0, 1, 2, 3, 4, 5, 10, 11. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        for (int i = 0; i < 14; i++) {
            (*test_arr_seq).Append(unsorted[i]);
        }
        QuickSort<ArraySequence<int>>(test_arr_seq, 0);
        for (int i = 0; i < 14; i++) {
            if (sorted[i] != (*test_arr_seq).Get(i)) {
                cout << "NOT PASSED\n";
                throw runtime_error("1. QuickSort is wrong.");
            }
        }
        cout << "PASSED\n";
        delete test_arr_seq;
        
        //2
        cout << "2. NULL sequence pointer. ";
        try {
            correct = 0;
            QuickSort<ArraySequence<int>>(nullptr, 0);
        } catch (const std::invalid_argument& excp) {
            correct = 1;
        }
        if (correct == 0) {
            cout << "NOT PASSED\n";
            throw runtime_error("2. QuickSort is wrong. NULL pointer case.");
        }
        cout << "PASSED\n";
        
        //3
        cout << "3. Sort sorted sequence 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 11, 11. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        for (int i = 0; i < 14; i++) {
            (*test_arr_seq).Append(sorted[i]);
        }
        QuickSort<ArraySequence<int>>(test_arr_seq, 0);
        for (int i = 0; i < 14; i++) {
            if (sorted[i] != (*test_arr_seq).Get(i)) {
                cout << "NOT PASSED\n";
                throw runtime_error("3. QuickSort is wrong.");
            }
        }
        cout << "PASSED\n";
        delete test_arr_seq;
        
        //4
        cout << "4. Sort mono sequence 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        for (int i = 0; i < 14; i++) {
            (*test_arr_seq).Append(mono[i]);
        }
        QuickSort<ArraySequence<int>>(test_arr_seq, 0);
        for (int i = 0; i < 14; i++) {
            if (mono[i] != (*test_arr_seq).Get(i)) {
                cout << "NOT PASSED\n";
                throw runtime_error("4. QuickSort is wrong.");
            }
        }
        cout << "PASSED\n";
        delete test_arr_seq;
        
        //5
        cout << "5. Sort sequence without elements. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        QuickSort<ArraySequence<int>>(test_arr_seq, 0);
        cout << "PASSED\n";
        delete test_arr_seq;
        
        printf("All tests are passed.\n\n\n");
    } catch (const runtime_error& re) {
        cout << "Runtime error: " << re.what() << endl;
    } catch (const exception& ex) {
        cout << "Error occurred: " << ex.what() << endl;
    } catch (...) {
        cout << "Unknown failure occurred. Possible memory corruption" << endl;
    }
    
    std::cout << "Tests for HeapSort\n";
    try {
        //1
        cout << "1. Sort sequence 11, 10, 9, 8, 7, 6, 0, 1, 2, 3, 4, 5, 10, 11. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        for (int i = 0; i < 14; i++) {
            (*test_arr_seq).Append(unsorted[i]);
        }
        HeapSort<ArraySequence<int>>(test_arr_seq, 0);
        for (int i = 0; i < 14; i++) {
            if (sorted[i] != (*test_arr_seq).Get(i)) {
                cout << "NOT PASSED\n";
                throw runtime_error("1. HeapSort is wrong.");
            }
        }
        cout << "PASSED\n";
        delete test_arr_seq;
        
        //2
        cout << "2. NULL sequence pointer. ";
        try {
            correct = 0;
            HeapSort<ArraySequence<int>>(nullptr, 0);
        } catch (const std::invalid_argument& excp) {
            correct = 1;
        }
        if (correct == 0) {
            cout << "NOT PASSED\n";
            throw runtime_error("2. HeapSort is wrong. NULL pointer case.");
        }
        cout << "PASSED\n";
        
        //3
        cout << "3. Sort sorted sequence 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 11, 11. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        for (int i = 0; i < 14; i++) {
            (*test_arr_seq).Append(sorted[i]);
        }
        HeapSort<ArraySequence<int>>(test_arr_seq, 0);
        for (int i = 0; i < 14; i++) {
            if (sorted[i] != (*test_arr_seq).Get(i)) {
                cout << "NOT PASSED\n";
                throw runtime_error("3. HeapSort is wrong.");
            }
        }
        cout << "PASSED\n";
        delete test_arr_seq;
        
        //4
        cout << "4. Sort mono sequence 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        for (int i = 0; i < 14; i++) {
            (*test_arr_seq).Append(mono[i]);
        }
        HeapSort<ArraySequence<int>>(test_arr_seq, 0);
        for (int i = 0; i < 14; i++) {
            if (mono[i] != (*test_arr_seq).Get(i)) {
                cout << "NOT PASSED\n";
                throw runtime_error("4. HeapSort is wrong.");
            }
        }
        cout << "PASSED\n";
        delete test_arr_seq;
        
        //5
        cout << "5. Sort sequence without elements. ";
        try {
            test_arr_seq = new ArraySequence<int>(14);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        HeapSort<ArraySequence<int>>(test_arr_seq, 0);
        cout << "PASSED\n";
        delete test_arr_seq;
        
        
        
        
        
        printf("All tests are passed.\n\n\n");
    } catch (const runtime_error& re) {
        cout << "Runtime error: " << re.what() << endl;
    } catch (const exception& ex) {
        cout << "Error occurred: " << ex.what() << endl;
    } catch (...) {
        cout << "Unknown failure occurred. Possible memory corruption" << endl;
    }
    
    cout << "Another tests.\n";
    try {
        cout << "File does not exist test.\n";
        FileTask<ArraySequence<int>>* test_file_task;
        try {
            test_file_task = new FileTask<ArraySequence<int>>(1,3,3,"/Users/SwanLiberal/Desktop/another_raw.csv","","");
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        (*test_file_task).Make();
        cout << "PASSED\n";
        delete test_file_task;
    } catch (const runtime_error& re) {
        cout << "Runtime error: " << re.what() << endl;
    } catch (const exception& ex) {
        cout << "Error occurred: " << ex.what() << endl;
    } catch (...) {
        cout << "Unknown failure occurred. Possible memory corruption" << endl;
    }
    return 0;
}
